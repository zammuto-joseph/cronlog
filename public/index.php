<?php

session_start ();

include ("../config/site.conf.php");
include ("../lib/meekrodb.class.php");
include ("../config/db.conf.php");
include ("../lib/data.php");

date_default_timezone_set("America/New_York");

if (isset ($_GET["page"]) && $_GET["page"] !== "") {
    $page = $_GET["page"];
} else {
    $page = "landing";
}
if (isset ($_POST["cardswipe"])) {
    $_SESSION["cardswipe"] = $_POST["cardswipe"];
    $_SESSION["insession"] = "true";
}
if (isset ($_GET["logoff"]) && $_GET["logoff"] == "true") {
    unset ($_SESSION);
    session_destroy ();
}
if (!isset ($_GET["page"])) {
    header ("Location: " . $site_config["url_base"] . "/index.php?page=" . $page);
}

?>

<html>
<head>
    <title>CronLog</title>
    <link rel="stylesheet" type="text/css" href="site.css" />
    <link rel="stylesheet" type="text/css" href="theme.css" />
</head>
<body>
    <div class="w3-theme-dark w3-card w3-xxlarge w3-center">
        CronLog
    </div>
    <?php include ("../pages/" . $page . ".php"); ?>
</body>
</html>