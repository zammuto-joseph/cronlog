<?php

$timelogdata = DATA::getUserTimes($_SESSION["cardinfo"]["badgenumber"]);
$totalhours = 0;
$shiftopened = FALSE;
if ($timelogdata[0] !== FALSE) {
    $timelogtable = '<table class="w3-table w3-border w3-striped"><tr><th>Start Time</th><th>End Time</th><th>Total Hours This Shift</th></tr>';
    foreach ($timelogdata[1] as $timelogentry) {
        if (strtotime ($timelogentry["shiftendtime"]) > 0) {
            $shifthours = (strtotime (DATA::roundTimeFifteen ($timelogentry["shiftendtime"])) - strtotime (DATA::roundTimeFifteen ($timelogentry["shiftstarttime"]))) / 60 / 60;
            $totalhours += $shifthours;
            $timelogtable .= '<tr><td>' . $timelogentry["shiftstarttime"] . '</td><td>' . $timelogentry["shiftendtime"] . '</td><td>' . $shifthours . '</td></tr>';
            $shiftopened = FALSE;
        } else {
            $timelogtable .= '<tr><td>' . $timelogentry["shiftstarttime"] . '</td><td>-</td><td>-</td></tr>';
            $shiftopened = TRUE;
        }
    }
    $timelogtable .= '</table>';
} else {
    $timelogtable = '<p class="w3-center">You have not logged any time yet.</p>';
}

?>
<div class="w3-center">
    <p class="w3-center w3-large">USER INFO:</p>
    <table class="w3-table w3-bordered w3-border">
        <tr>
            <th>First Name:</th>
            <td><?php echo $_SESSION["cardinfo"]["firstname"]; ?></td>
            <th>Last Name:</th>
            <td><?php echo $_SESSION["cardinfo"]["lastname"]; ?></td>
        </tr>
        <tr>
            <th>Badge Number:</th>
            <td><?php echo $_SESSION["cardinfo"]["badgenumber"]; ?></td>
            <th>Badge Name:</th>
            <td><?php echo $_SESSION["cardinfo"]["badgename"]; ?></td>
        </tr>
        <tr>
            <th>Department:</th>
            <td><?php echo $_SESSION["cardinfo"]["department"]; ?></td>
            <th>Position:</th>
            <td><?php echo $_SESSION["cardinfo"]["position"]; ?></td>
    </table>
</div>
<div class="w3-theme-d2 w3-bar w3-large w3-center">
    Total logged hours: <?php echo $totalhours; ?>
</div>
<?php

echo $timelogtable;
if ($shiftopened) {
    $inout = "OUT";
    $action = "clockout";
} else {
    $inout = "IN";
    $action = "clockin";
}

?>

<div class="w3-theme-d2 w3-bar w3-padding w3-center">
    <a href="<?php echo $site_config["url_base"]; ?>/index.php?page=landing&logoff=true" class="w3-button w3-theme-light">GO BACK</a>
    <a href="<?php echo $site_config["url_base"]; ?>/index.php?page=submittimeentry&action=<?php echo $action; ?>" class="w3-button w3-theme-light">CLOCK<?php echo " " . $inout; ?></a>
</div>