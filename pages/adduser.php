<?php



?>

<div class="w3-center">
    <table class="w3-table w3-bordered w3-border">
        <tr>
            <th>First Name:</th>
            <td><?php echo $_SESSION["cardinfo"]["firstname"]; ?></td>
            <th>Last Name:</th>
            <td><?php echo $_SESSION["cardinfo"]["lastname"]; ?></td>
        </tr>
        <tr>
            <th>Badge Number:</th>
            <td><?php echo $_SESSION["cardinfo"]["badgenumber"]; ?></td>
            <th>Badge Name:</th>
            <td><?php echo $_SESSION["cardinfo"]["badgename"]; ?></td>
        </tr>
        <tr>
            <th>Department:</th>
            <td><?php echo $_SESSION["cardinfo"]["department"]; ?></td>
            <th>Position:</th>
            <td><?php echo $_SESSION["cardinfo"]["position"]; ?></td>
    </table>
    <p class="w3-red">This user does not yet exist in CronLog. Would you like to add them?</p>
</div>

<div class="w3-theme-d2 w3-bar w3-center">
    <a class="w3-button w3-theme-light" href="<?php echo $site_config["url_base"]; ?>/index.php?page=submitnewuser">ADD USER</a>
    <a class="w3-button w3-theme-light" href="<?php echo $site_config["url_base"]; ?>/index.php?page=landing&logoff=true">CANCEL</a>
</div>