<?php

if (!isset ($_SESSION["insession"]) || $_SESSION["insession"] !== "true") {
    header("Location: " . $site_config["url_base"] . "/index.php?page=identify");
} else {
    $_SESSION["cardinfo"] = DATA::explodeBadgeString($_SESSION["cardswipe"]);
    $user = DATA::queryUser($_SESSION["cardinfo"]["badgenumber"]);
    if ($user !== FALSE) {
        header("Location: " . $site_config["url_base"] . "/index.php?page=logtime");
    } else {
        header("Location: " . $site_config["url_base"] . "/index.php?page=adduser");
    }
}

?>