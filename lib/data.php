<?php

class DATA {

    function explodeBadgeString ($badgestring) {
        $badgestring = substr ($badgestring, 1, -1);
        $rawdata = explode ("^", $badgestring);
        $output = array (
            "badgenumber" => $rawdata[0],
            "badgename" => $rawdata[1],
            "firstname" => $rawdata[2],
            "lastname" => $rawdata[3],
            "department" => $rawdata[4],
            "position" => $rawdata[5]
        );
        return $output;
    }
    function roundTimeFifteen ($timestamp) {
        $timestamp = strtotime ($timestamp);
        $precision = 60 * 15;
        return date("Y-m-d H:i:s", round ($timestamp / $precision) * $precision);
    }
    function queryUser ($badgenumber) {
        global $db_config;
        $t_user_result = DB::queryFirstRow ("SELECT * FROM " . $db_config["name"] . "." . $db_config["users_table"] . " WHERE badgenumber = %s", $badgenumber);
        if (empty ($t_user_result)) {
            return FALSE;
        } else {
            return $t_user_result;
        }
    }
    function addUser ($badgeinfo) {
        global $db_config;
        $userfound = DATA::queryUser ($badgeinfo["badgenumber"]);
        if ($userfound === FALSE) {
            DB::insert ($db_config["name"] . "." . $db_config["users_table"],
                array (
                    "firstname" => $badgeinfo["firstname"],
                    "lastname" => $badgeinfo["lastname"],
                    "badgename" => $badgeinfo["badgename"],
                    "badgenumber" => $badgeinfo["badgenumber"],
                    "department" => $badgeinfo["department"],
                    "position" => $badgeinfo["position"]
                )
            );
            return TRUE;
        } else {
            return array ($badgenumber, $badgename);
        }
    }
    function queryTimes ($userid) {
        global $db_config;
        $t_shifts_results = DB::query ("SELECT * FROM " . $db_config["name"] . "." . $db_config["shift_table"] . " WHERE userid = %i", $userid);
        if (empty ($t_shifts_results)) {
            return FALSE;
        } else {
            return $t_shifts_results;
        }
    }
    function queryLastTime ($userid) {
        global $db_config;
        $t_shifts_result = DB::queryFirstRow ("SELECT * FROM " . $db_config["name"] . "." . $db_config["shift_table"] . " WHERE userid = %i ORDER BY shiftid DESC", $userid);
        if (empty ($t_shifts_result)) {
            return FALSE;
        } else {
            return $t_shifts_result;
        }
    }
    function getUserTimes ($badgenumber) {
        $output = array (FALSE);
        $userqueryresult = DATA::queryUser ($badgenumber);
        if ($userqueryresult !== FALSE) {
            $userid = $userqueryresult["id"];
            $timequeryresult = DATA::queryTimes ($userid);
            if ($timequeryresult !== FALSE) {
                $output = array(TRUE, $timequeryresult);
            }
        }
        return $output;
    }
    function setUserTimes ($badgenumber, $time, $inout) {
        global $db_config;
        $userqueryresult = DATA::queryUser ($badgenumber);
        $userid = $userqueryresult["id"];
        if ($inout == "in") {
            DB::insert ($db_config["name"] . "." . $db_config["shift_table"],
                array (
                    "userid" => $userid,
                    "shiftstarttime" => $time
                )
            );
        }
        if ($inout == "out") {
            $output = "";
            $lasttimequeryresult = DATA::queryLastTime ($userid);
            $lasttimein = $lasttimequeryresult["shiftstarttime"];
            $shiftid = $lasttimequeryresult["shiftid"];
            $timedifference = strtotime ($time) - strtotime ($lasttimein);
            DB::update ($db_config["name"] . "." . $db_config["shift_table"],
                array ("shiftendtime" => $time),
                "shiftid = %i",
                $shiftid
            );
            if ($timedifference > 12 * 60) {
                return "longshift";
            }
        }
    }
    function clockIn ($badgenumber) {
        $clockintime = date ("Y-m-d H:i:s");
        DATA::setUserTimes ($badgenumber, $clockintime, "in");
    }
    function clockOut ($badgenumber) {
        $clockouttime = date ("Y-m-d H:i:s");
        DATA::setUserTimes ($badgenumber, $clockouttime, "out");
    }
}

?>